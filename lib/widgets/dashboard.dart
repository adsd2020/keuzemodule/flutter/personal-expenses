import 'dart:io';

import 'package:flutter/material.dart';

import './transaction_list.dart';
import './transaction_form.dart';
import './chart.dart';

import '../models/transaction.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  var _showChart = false;
  final List<Transaction> _transactions = [
    Transaction(
        id: 1, title: 'First transaction', amount: 20.50, date: DateTime.now()),
    Transaction(
        id: 2,
        title: 'Second transaction',
        amount: 100.99,
        date: DateTime.now().subtract(const Duration(days: 2))),
    Transaction(
        id: 3,
        title: 'Third transaction',
        amount: 50.25,
        date: DateTime.now().subtract(const Duration(days: 3))),
    Transaction(
        id: 4,
        title: 'Third transaction',
        amount: 10.90,
        date: DateTime.now().subtract(const Duration(days: 1))),
    Transaction(
        id: 5,
        title: 'Third transaction',
        amount: 50.25,
        date: DateTime.now().subtract(const Duration(days: 5))),
    Transaction(
        id: 6,
        title: 'Third transaction',
        amount: 50.25,
        date: DateTime.now().subtract(const Duration(days: 1))),
    Transaction(
        id: 7, title: 'Third transaction', amount: 50.25, date: DateTime.now()),
  ];

  List<Transaction> get _recentTransactions {
    return _transactions
        .where(
          (transaction) => transaction.date.isAfter(
            DateTime.now().subtract(
              const Duration(days: 7),
            ),
          ),
        )
        .toList();
  }

  void startAddingTransaction() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (_) {
          return GestureDetector(
            onTap: () {},
            child: TransactionForm(addTransaction: _addTransaction),
          );
        });
  }

  void _addTransaction(String title, double amount, DateTime date) {
    final _transaction = Transaction(
      id: _transactions.isNotEmpty ? _transactions.last.id + 1 : 1,
      title: title,
      amount: amount,
      date: date,
    );

    setState(() {
      _transactions.add(_transaction);
    });

    Navigator.of(context).pop();
  }

  void _deleteTransaction(int id) {
    setState(() {
      _transactions.removeWhere((tx) => tx.id == id);
    });
  }

  List<Widget> _buildPortraitContent(
    MediaQueryData _mediaQuery,
    AppBar _appBar,
  ) {
    return [
      Container(
        height: (_mediaQuery.size.height -
                _appBar.preferredSize.height -
                _mediaQuery.padding.top) *
            0.3,
        child: Chart(recentTransactions: _recentTransactions),
      ),
      Container(
        height: (_mediaQuery.size.height -
                _appBar.preferredSize.height -
                _mediaQuery.padding.top) *
            0.7,
        child: _transactions.isEmpty
            ? LayoutBuilder(builder: (context, constraints) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      'No transaction added yet!',
                      style: Theme.of(context).textTheme.headline6,
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 20),
                      height: constraints.maxHeight * 0.7,
                      child: Image.asset(
                        'assets/images/waiting.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                  ],
                );
              })
            : TransactionList(
                transactions: _transactions,
                deleteTransaction: _deleteTransaction,
              ),
      )
    ];
  }

  Widget _buildLandscapeContent(
    MediaQueryData _mediaQuery,
    AppBar _appBar,
  ) {
    return _showChart
        ? Container(
            height: (_mediaQuery.size.height -
                    _appBar.preferredSize.height -
                    _mediaQuery.padding.top) *
                0.7,
            // 0.3,
            child: Chart(recentTransactions: _recentTransactions),
          )
        : Container(
            height: (_mediaQuery.size.height -
                    _appBar.preferredSize.height -
                    _mediaQuery.padding.top) *
                0.7,
            child: _transactions.isEmpty
                ? LayoutBuilder(builder: (context, constraints) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          'No transaction added yet!',
                          style: Theme.of(context).textTheme.headline6,
                          textAlign: TextAlign.center,
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 20),
                          height: constraints.maxHeight * 0.7,
                          child: Image.asset(
                            'assets/images/waiting.png',
                            fit: BoxFit.contain,
                          ),
                        ),
                      ],
                    );
                  })
                : TransactionList(
                    transactions: _transactions,
                    deleteTransaction: _deleteTransaction,
                  ),
          );
  }

  @override
  Widget build(BuildContext context) {
    final _mediaQuery = MediaQuery.of(context);
    final _isLandscape = _mediaQuery.orientation == Orientation.landscape;
    final appBar = AppBar(
      title: const Text('Personal Expenses App'),
      actions: [
        IconButton(
          onPressed: () => startAddingTransaction(),
          icon: const Icon(
            Icons.add,
            size: 35,
          ),
        )
      ],
    );

    return Scaffold(
      appBar: appBar,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            if (_isLandscape)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('Show Chart'),
                  Switch.adaptive(
                    activeColor: Theme.of(context).colorScheme.secondary,
                    value: _showChart,
                    onChanged: (value) {
                      setState(() {
                        _showChart = value;
                      });
                    },
                  ),
                ],
              ),
            if (!_isLandscape) ..._buildPortraitContent(_mediaQuery, appBar),
            if (_isLandscape) _buildLandscapeContent(_mediaQuery, appBar),
          ],
        ),
      ),
      floatingActionButton: Platform.isIOS || Platform.isMacOS
          ? Container()
          : FloatingActionButton(
              child: const Icon(Icons.add),
              onPressed: () => startAddingTransaction(),
            ),
    );
  }
}
