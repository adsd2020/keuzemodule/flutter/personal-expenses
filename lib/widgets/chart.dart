import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import './chart_bar.dart';

import '../models/transaction.dart';
import '../helpers/date.dart';

class Chart extends StatelessWidget {
  final List<Transaction> recentTransactions;

  Chart({Key? key, required this.recentTransactions}) : super(key: key);

  Map<String, Object> get groupedTransactionValues {
    var totalSum = 0.0;
    final List<Map<String, Object>> weekData;

    weekData = List.generate(7, (index) {
      final weekDay = DateTime.now().subtract(Duration(days: index));
      var dayTotal = 0.0;

      for (Transaction transaction in recentTransactions) {
        // this not workt and difference will give wrong amount because of time difference
        // Write a Date class with static helper function to parse a date with zero time values
        if (Date.parse(transaction.date)
                .difference(Date.parse(weekDay))
                .inDays ==
            0) {
          dayTotal += transaction.amount;
          totalSum += transaction.amount;
        }
        // if (transaction.date.day == weekDay.day && transaction.date.month == weekDay.month && transaction.date.year == weekDay.year) {
        //   totalSum += transaction.amount;
        // }
      }
      return {'day': DateFormat.E("nl").format(weekDay), 'amount': dayTotal};
    });
    return {'total': totalSum, 'weekData': weekData.reversed.toList()};
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: (groupedTransactionValues['weekData'] as List).map((data) {
            return ChartBar(
                label: data['day'].toString(),
                spendingAmount: data['amount'] as double,
                totalSpending: groupedTransactionValues['total'] as double);
          }).toList(),
        ),
      ),
    );
  }
}
