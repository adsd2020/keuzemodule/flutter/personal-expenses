import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import './transaction_card.dart';

import '../models/transaction.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final Function deleteTransaction;

  const TransactionList(
      {Key? key, required this.transactions, required this.deleteTransaction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        return Card(
          elevation: 6,
          margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
          child: ListTile(
            leading: CircleAvatar(
              radius: 30,
              child: Padding(
                padding: const EdgeInsets.all(6),
                child: FittedBox(
                  child: Text(NumberFormat("€ #,##0.00")
                      .format(transactions[index].amount)),
                ),
              ),
            ),
            title: Text(
              transactions[index].title,
              // style: const TextStyle(
              //   fontWeight: FontWeight.w800,
              //   fontSize: 16,
              // ),
              style: Theme.of(context).textTheme.headline6,
            ),
            subtitle: Text(
              DateFormat('dd MMMM yyyy', 'nl').format(transactions[index].date),
              style: const TextStyle(color: Colors.grey),
            ),
            trailing: MediaQuery.of(context).size.width > 460
                ? TextButton.icon(
                    onPressed: () {},
                    icon: Icon(
                      Icons.delete,
                      color: Theme.of(context).errorColor,
                    ),
                    label: const Text('Delete transaction'),
                    style: TextButton.styleFrom(
                      primary: Theme.of(context).colorScheme.secondary,
                      backgroundColor: Theme.of(context).colorScheme.primaryVariant,
                    ),
                  )
                : IconButton(
                    icon: Icon(
                      Icons.delete,
                      color: Theme.of(context).errorColor,
                    ),
                    onPressed: () => deleteTransaction(transactions[index].id),
                  ),
          ),
        );
        // return TransactionCard(
        //   transaction: transactions[index],
        // );
      },
      itemCount: transactions.length,
    );
  }
}
