class Date{
  static DateTime parse(DateTime date){
    return DateTime(date.year, date.month, date.day);
  }
}